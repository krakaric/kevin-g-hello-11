terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
      }
    }
    backend "http" {
    }
}

provider "aws" {
  region = "us-west-2"

}

resource "aws_s3_bucket" "kevin-bucket1" {
  bucket = "kevin-bucket1"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_public_access_block" "bucket-public-access-block" {
  bucket = aws_s3_bucket.kevin-bucket1.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.kevin-bucket1.id
  key    = "g-hello.jar"
  source = "build/libs/g-hello-11-0.0.1-SNAPSHOT.jar"
}

resource "aws_vpc" "kevin-vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  
  tags = {
    Name = "kevin-vpc"
  }
}

resource "aws_subnet" "kevin-public-subnet" {
  vpc_id = aws_vpc.kevin-vpc.id
  cidr_block = "10.0.1.0/24"

tags = {
    name = "kevin-public-subnet"
  }
}

resource "aws_subnet" "kevin-private-subnet" {
  vpc_id = aws_vpc.kevin-vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    name = "kevin-private-subnet"
  }
}

resource "aws_internet_gateway" "kevin-gateway" {
  vpc_id = aws_vpc.kevin-vpc.id

  tags = {
    name = "Kevin VPC IG"
  }
}

resource "aws_route_table" "kevin_rt" {
    vpc_id = aws_vpc.kevin-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.kevin-gateway.id
    }

    tags = {
        name = "kevin_rt"
    }
}

resource "aws_route_table_association" "public_subnet" {
    subnet_id = aws_subnet.kevin-public-subnet.id
    route_table_id = aws_route_table.kevin_rt.id
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id = aws_vpc.kevin-vpc.id

  ingress {
    description      = "spring boot port"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description      = "ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_iam_role_policy" "test_policy" {
  name = "test_policy"
  role = aws_iam_role.test_role1.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:Describe*",
          "s3:GetObject",
        ]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::kevin-bucket1/*"
      },
    ]
  })
}

resource "aws_iam_role" "test_role1" {
  name = "test_role1"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_instance_profile" "kevin_test_profile" {
  name = "kevin_test_profile"
  role = aws_iam_role.test_role1.name
}

resource "aws_instance" "web" {
  ami = "ami-0df24e148fdb9f1d8"
  instance_type = "t3.micro"
  iam_instance_profile = aws_iam_instance_profile.kevin_test_profile.name
  key_name = "kevin-keypair"
  vpc_security_group_ids = [
    aws_security_group.allow_tls.id
    ]
  subnet_id = aws_subnet.kevin-public-subnet.id
  associate_public_ip_address = true
  tags = {
    Name = "kevin-g-hello"
  }
user_data = <<EOF
#! /bin/bash
sudo yum update -y
sudo yum install -y java-17-amazon-corretto-headless
aws s3api get-object --bucket "kevin-bucket1" --key "g-hello.jar" "g-hello.jar"
java -jar g-hello.jar
EOF
}

output "public_ipv4_dns" {
    description = "public IPv4 DNS of the EC2 instance"
    value       = aws_instance.web.public_dns
}

output "api_base_url" {
    description = "public IPv4 DNS of the EC2 instance"
    value       = "http://${aws_instance.web.public_dns}:8080/hello"
}